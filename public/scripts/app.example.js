class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    console.log('ini button');
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    Car.list.forEach((car) => {
      // console.log("ini carslist",car);
      // node.innerHTML = car.render();
      const node = document.createElement("div");
        // console.log('ini node', node);
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
      const filterData = [];
      for (let i = 0; i< car.length; i++) {
        let data = document.createElement("div");
        data.classList.add("data","hide");
        if(car[i].capacity > 1 ) {
          filterData.push(car[i]);
        }
      }
      console.log('ini hasil capacity', filterData);
      return filterData;
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
