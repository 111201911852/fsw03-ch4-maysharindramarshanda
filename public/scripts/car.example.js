class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
      <div class="col-md-3 col-lg-3 mt-4 gy-4 my-3">
        <div class="card">
          <img src="${this.image}" alt="${this.manufacture}" class="card-img-top">
          <div class="card-body">
            <h6>Nama/Tipe Mobil</h6>
            <p><b>${this.description}</b></p>
            <p><i class="bi bi-people"></i><b>${this.capacity}</b></p>
            <p><i class="bi bi-gear"></i><b>${this.transmission}</b></p>
            <p><i class="bi bi-calendar"></i> Tahun <b>${this.year}</b></p>
            <hr>
            <a href="#" class="text-decoration-none text-center card-link detail " data-bs-toggle="#" data-bs-target="#exampleModal" ">Pilih Mobil</a>
          </div>
        </div>
      </div>
    `;
  }
}
