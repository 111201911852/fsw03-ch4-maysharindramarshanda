/**
 * Impor HTTP Standar Library dari Node.js
 * Hal inilah yang nantinya akan kita gunakan untuk membuat
 * HTTP Server
 * */
import http from "http";
import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";

// import {__dirname} from 'module';
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const directory = path.join(__dirname, '../public'); 

const port = 8000;

http 
  .createServer((req, res) => {
    switch (req.url) {
      case "/":
        req.url = "/index.html";
        break;
        case "/cars":
        req.url = "/cars.html";
        break;
    }
    console.log(req.url);
    let path = directory + req.url;
    fs.readFile(path, (err, data) => {
      res.writeHead(200);
      res.end(data);
    });
})
.listen(port, () => {
  console.log(`Server listening on port ${port}`);
})